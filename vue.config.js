module.exports = {
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false,
    },
    i18n: {
      locale: 'tw',
      fallbackLocale: 'tw',
      localeDir: 'locales',
      enableInSFC: false,
    },
    electronBuilder: {
      nodeIntegration: true,
    },
  },
  transpileDependencies: [
    'quasar',
  ],
};
